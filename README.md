# Runner

## 部署步骤

```bash
git clone https://github.com/HoggAdams/runner.git
chmod +x install.sh
./install.sh
```

## 指令参数介绍

**Usage:**

​    runner [options]				Operation

​    runner [options] [file ...]     Operation file 

​    runner [options] [options]		Interesting parameter

**Options:**

--				Author

-u  			Compile programming files

--user



-r 				remove files

--remove 

​    -re			Empty the files in the trash

​    --remove-empty

​    -rr 		Restore the deleted file.

​    --remove-restore

​    -r[N]		Remove Files
​           -remove[Files]



-g 				Some Interesting Games

--game



-h				runner's help

--help

## 作者



Author：Hogg Adams
